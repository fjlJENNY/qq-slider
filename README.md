# vue-demo

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



### 
See [touchEvent](https://developer.mozilla.org/en-US/docs/Web/API/Touch_events)